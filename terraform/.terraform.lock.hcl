# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/yandex-cloud/yandex" {
  version     = "0.90.0"
  constraints = "0.90.0"
  hashes = [
    "h1:2mkOPN+DrypV8dREOyQBsnLb1PYdrl9h+fMA6vzPDLw=",
  ]
}
